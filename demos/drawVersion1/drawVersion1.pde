void setup() {
  size(400, 400);
  background(255);
}

int fillValue = 0;
  
void draw() {
  stroke(0);
  line(pmouseX, pmouseY, mouseX, mouseY);
}


void keyPressed(){
   if(keyCode == 45){
     if(fillValue > 10){
         fillValue = fillValue - 10; 
     }
     else{
       fillValue = 0;
     }
     println ("Fill Value: " + fillValue);
   }
   else if(keyCode == 61){
     if(fillValue < 245){
         fillValue = fillValue + 10;
     }
     else{
       fillValue = 255;
     }
     println ("Fill Value: " + fillValue);
   }
}

void mousePressed() {
  println ("MOUSE FILL VALUE: " + fillValue);
  if(mouseButton == 37){
      stroke(0);
      fill(fillValue);
      rectMode(CENTER);
      rect(mouseX, mouseY, 16, 16);
  }
  else if(mouseButton == 39){
      stroke(0);
      fill(fillValue);
      ellipse(mouseX, mouseY, 10, 10);
  }
  
  println ( mouseButton );
}