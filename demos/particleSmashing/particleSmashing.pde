ArrayList poop;
boolean flag=true;
int distance=100;

void setup(){
  size(1000, 300);
  smooth();
  poop = new ArrayList();
  for (int i=0; i<100; i++){
    Particle P = new Particle();
    poop.add(P);
  }
}

void draw()   // "the function will not return anything, thus, there is no need for a data type" so no need to draw?
{
  background(-1);
  for (int i=0; i<poop.size(); i++){
    Particle Pn1 = (Particle) poop.get(i); // draw particles?
    Pn1.display();
    Pn1.update();
    for (int j = i + 1; j < poop.size(); j++) {
      Particle Pn2 = (Particle) poop.get(j);
      Pn2.update();
      
      if (dist(Pn1.x, Pn1.y, Pn2.x, Pn2.y)< distance){  // if 3 particles meet, form triangles?
        for (int k = j + 1; k < poop.size(); k++) {
          Particle Pn3 = (Particle) poop.get(k);
          if (dist(Pn3.x, Pn3.y, Pn2.x, Pn2.y)< distance) {
            if (flag) {
              stroke(255, 10);
              fill(Pn3.c, 15); // method to access the class property
            } else {
              noFill();
              strokeWeight(1);
              stroke(0, 20);
            }
            beginShape(TRIANGLES);
            vertex(Pn1.x, Pn1.y);
            vertex(Pn2.x, Pn2.y);
            vertex(Pn3.x, Pn3.y);
            endShape();
          }

          Pn3.update();
        }
      }
    }
  }
}

void keyPressed()
{
  flag=!flag;
}