class Particle {  // change triangle colors
  float x, y, r;
  color c;
  int i=1, j=1;
  
  Particle( ){
    x = random(0, width);
    y = random(0, height);
    r = random(1, 5);
    int j = (int)random(0, 4);
    if (j==0){
      c = color(#05CDE5);
    }
    if (j==1){
      c = color(#FFB803);
    }
    if (j==2){
      c = color(#FF035B);
    }
    if (j==3){
      c = color(#3D3E3E);
    }
  }

  void display(){
    pushStyle();  //"The pushStyle() function saves the current style settings and popStyle() restores the prior settings." So it keeps the particles in shape after they meet others?
    noStroke();
    fill(c);
    ellipse(x, y, r, r);
    popStyle();
  }

  void update(){      // change the directions after particles hit the edge
    x = x + j*0.01; //speed
    y = y + i*0.01;
    if (y > height-r) i=-1;
    if (y < 0+r) i=1;
    if (x > width-r) j=-1;
    if (x < 0+r) j=1;
  }
}