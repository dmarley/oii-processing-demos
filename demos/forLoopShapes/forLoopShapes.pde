void setup(){
  size(300,300);
  background(255);
}

void draw(){
  noFill();
  for (int i=0; i < 10; i++){
    rect(i*20, height/2, 5,5);
  }
  
  int i = 0;
  while(i < 10 ){
      ellipse(width/2, height/2, i*10, i*20);
      i++;
  }

  for(float x = 1.0; x < width; x *=1.1){
    rect (0, x, x, x*2);
  }
  
  int x = 0;
  for (int c = 255; c > 0; c -= 15){
    fill(c);
    rect(x, height - 25, 10,10);
    x = x + 10;
  }
}