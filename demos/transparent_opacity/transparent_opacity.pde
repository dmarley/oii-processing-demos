size(400,400);

background(0);
noStroke();

fill(0, 0, 255);  // no 4th argument means 100% opacity (solid colour)
rect(0, 0 , 100 , 200);

fill(255, 0, 0, 255);  // 4th argument means 100% opacity (solid colour)
rect(0, 0 , 200 , 40);

fill(255, 0, 0, 191);  // 4th argument means 100% opacity (solid colour)
rect(0, 50, 200 , 40);

fill(255, 0, 0, 127);  // 50% opacity
rect(0, 100, 200, 40);

fill(255, 0, 0, 63);  // 25% opacity
rect(0, 150, 200, 40);