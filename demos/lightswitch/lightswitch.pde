//Click on the box to turn light on and off

boolean button = false;

int x = 50;
int y = 50;
int width = 100;
int height = 75;


void setup(){
  size(480,480);
}

void draw(){
    if(button == true){
      background(255);
    }
    else{
      background(0);
    }
    
    fill(175);
    rect(x,y,width,height);
}

void mousePressed(){
    setButton();
}

boolean setButton(){
  if(mouseX > x && mouseX < x+ width && mouseY > y && mouseY < y+height){
    button = !button;
  }
  return button;
}