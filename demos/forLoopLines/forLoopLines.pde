void setup(){
  //size(300,300);
  background(157,45,5);
}

void draw(){
    for(int i = 1; i < 26; i++){
        stroke(255,45,25);
        line(0, i*4, i*4, i*4);
        stroke(34,56,255);
        line(i*4,0,i*4,i*4);
    }
  // Save as image
  saveFrame("output.png");
}