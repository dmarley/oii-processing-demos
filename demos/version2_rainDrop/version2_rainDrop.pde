Drop[] drops = new Drop[500];

void setup(){
  size(400,400);
  //d = new Drop();
  for (int i = 0; i < drops.length; i++){
    drops[i] = new Drop();
  }
}

void draw(){
  
    background(230,230,250);
    
    // Show Random Rain
    for (int i = 0; i < drops.length; i++){
        drops[i].fall();
        drops[i].show();
    }
    
}