void setup() {
  size(200, 200);
}

void draw() {
  //head colour
  fill(233, 234, 201);

  //head
  ellipse(100, 100, 150, 150);

  //white
  fill(225);

  //eyes
  ellipse(75, 85, 30, 20);
  ellipse(125, 85, 30, 20);

  //black
  fill(0);

  //pupils
  ellipse(75, 85, 10, 10);
  ellipse(125, 85, 10, 10);

  //red
  fill(225, 0, 0);

  //mouth
  arc(100, 125, 80, 50, 0, 3.14);
  line(60, 125, 140, 125);
}