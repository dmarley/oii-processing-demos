//This is a single line comment,
and this is not. This will cause an error.
/* This is a multi line comment,
and it allows for things like this.
However, it will go on as long as you don't end it,
like */ this, which causes an error.