// the big smoke of circles
noStroke();
fill(255,0,0);
ellipse(10,10,50,50);

// purple
noStroke();
fill(255,0,255);
ellipse(40,40,15,15);

// green
noStroke();
fill(0,255,0);
ellipse(45,20,15,15);

/*
red: 255,0,0
green: 0,255,0
blue: 0,0,255
*/