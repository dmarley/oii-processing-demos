//myval= makes a variable
//void keyPressed(w){} causes the w key to activated the line of code in {}
//left click is 37 right click is 39 scroll weel click is 3
// visit processing.org for list of commands
int line=10;
int square=0;
void setup() {
  size(500, 500);
  frameRate(50);
  background(255);
}


void draw() {
  stroke(0);
  if (mouseButton==39) {
    stroke(random(255), random(255), random(255));
    /*stroke(random(255),random(255),random(255)); causes the line coulor to
     constantly change coulors*/
    strokeWeight(line);
    line(pmouseX, pmouseY, mouseX, mouseY);
  }
  //right click and hold allows you to draw a line
}


void mousePressed() {

  if (mouseButton==37) {
    strokeWeight(square);
    fill(200);
    rectMode(CENTER);
    rect(mouseX, mouseY, 16, 16);
  }
  println(mouseButton=37);
  // makes mouse button 37/left click the only key to make a square
}
void keyPressed() {
  println(keyCode);
  println(key);

  if (keyCode==61) {
    line=line+3;
    square=square+3;
  }
  if (keyCode==45) {
    line=line-3;
    square=square-3;
  }
}

/*i will type the letter/symbol/number then the key code
 a 65
 b 66
 c 67
 d 68
 e 69
 f 70
 g 71
 h 72
 i 73
 j 74
 k 75
 l 76
 m 77
 n 78
 o 79
 p 80
 q 81
 r 82
 s 83
 t 84
 u 85
 v 86
 w 87
 x 88
 y 89
 z 90
 */