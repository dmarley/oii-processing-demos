boolean moveForward = true;
int circleX=0;
int circleY=100;
void setup() {
  size(480, 270);
}
void draw() {
  background(255);
  stroke(0);
  fill(random(255), random(255), random(255));
  println(circleX);

  //use the variables tospecify the location of the ellipse.
  ellipse(circleX, circleY, 50, 50);
  //an assignment operation that increments the value of circleX by 1.

  if (circleX > (width - 25)) {
    moveForward = false;
  } else if (circleX < 25) {
    moveForward = true;
  }
  if (moveForward==true) {
    stroke(random(255), random(255), random(255));
    circleX++;
  } else {
    circleX--;
  }
}