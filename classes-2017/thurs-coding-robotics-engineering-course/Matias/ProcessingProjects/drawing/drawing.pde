int weight = 10;
void setup() {
  size(500, 500);
  background(255);
  frameRate(100);
}
void draw() { //Do nothing when app starts - waiting for events
  stroke(random(255), random(255), random(255));
  strokeWeight(weight);
  line(pmouseX, pmouseY, mouseX, mouseY);
}
void keyPressed() {
  if (keyCode==61) {
    weight = weight+1;
    println(keyCode);
    println(key);
  } else if (keyCode==61) {
    weight = weight - 1;
  }
}

  void mousePressed() {
    if (mouseButton==37) {
      stroke(random(255), random(255), random(255));
      fill(random(255), random(255), random(255));
      rectMode(CENTER);
      rect(mouseX, mouseY, 16, 16);
    }
    if (mouseButton==39) {
      stroke(random(255), random(255), random(255));
      ellipseMode(CENTER);
      ellipse(mouseX, mouseY, 16, 16);
    }
  }