int weight = 10;
void setup() {
  size(400, 400);
  background(255);
  frameRate(30);
}

void draw() {
  stroke(random(255), random(255), random(255));
  strokeWeight(weight);
  line(pmouseX, pmouseY, mouseX, mouseY);
}

void keyPressed() {
  if (keyCode == 61) {
    weight = weight + 1;
  } else if (keyCode == 45) {
    weight = weight - 1;
  }
}

void mousePressed() {
  if (mouseButton == 37) {
    stroke(random(255), random(255), random(255));
    fill(random(255), random(255), random(255));
    rectMode(CENTER);
    rect(mouseX, mouseY, 25, 25);
    println(mouseButton);
  }
  if (mouseButton == 39) {
    stroke(random(255), random(255), random(255));
    fill(random(255), random(255), random(255));
    ellipseMode(CENTER);
    ellipse(mouseX, mouseY, 25, 25);
  }
}