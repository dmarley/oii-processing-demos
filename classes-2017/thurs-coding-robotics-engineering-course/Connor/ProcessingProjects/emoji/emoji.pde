size(200, 200);
//head
fill(233, 255, 0);
ellipse(100, 100, 150, 150);
//eyes
fill(255);
ellipse(75, 85, 30, 20);
ellipse(125, 85, 30, 20);
//pupils
fill(0);
ellipse(75, 85, 10, 10);
ellipse(125, 85, 10, 10);
//mouth
fill(255, 0, 0);
arc(100, 125, 80, 50, 0, PI);
line(60, 125, 140, 125);
//hair
fill(147,91,6);
arc(100,70,150,100,3,6.5);