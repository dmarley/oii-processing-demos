int weight = 10;

void setup() {
  size(400, 400);
  background(255);
  frameRate(100);
}

void draw() {
  strokeWeight(weight);
  stroke(0);
  line(pmouseX, pmouseY, mouseX, mouseY);
}

void keyPressed() {
  println (keyCode);
  
  
  if (keyCode==61) { //Plus Key
    weight = weight + 1;
  } 
  else if (keyCode==45) { //Minus Key
    weight = weight - 1;
  }
}

void mousePressed() {
  if (mouseButton==37) {
    stroke(0); 
    fill(175);
    rectMode(CENTER);
    rect(mouseX, mouseY, 16, 16);
    println(mouseButton) ;
  }
}