size(200, 200);

//head colour
fill(0, 225, 225);

//head
ellipse(100, 100, 150, 150);

//white
fill(225);

//eyes
ellipse(75, 85, 30, 20);
ellipse(125, 85, 30, 20);

//black
fill(0);

//pupils
ellipse(75, 85, 10, 10);
ellipse(125, 85, 10, 10);

//red
fill(225, 0, 0);

//mouth
arc(100, 125, 80, 50, 0, 3.14);
line(60, 125, 140, 125);

//hair
line(120, 30, 120, 20); 