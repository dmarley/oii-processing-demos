int STROKEWEIGHT=20;

void setup() {
  size( 500, 500);
  frameRate(60);
  background(255);
}

void keyPressed() {
  println(keyCode);
  if (keyCode==61) {
    STROKEWEIGHT=STROKEWEIGHT+1;
  }
}

void draw() {
  strokeWeight(STROKEWEIGHT);
  stroke(random(255), random(255), random(255));
  line(pmouseX, pmouseY, mouseX, mouseY);
}


/*
void mousePressed() {
 if (mouseButton==37) {
 stroke(0);
 fill(175);
 rectMode(CENTER);
 rect(mouseX, mouseY, 16, 16);
 println(mouseButton);
 
 }
 
 }
 
 // line(pmouseX, pmouseY, mouseX, mouseY)};
 */