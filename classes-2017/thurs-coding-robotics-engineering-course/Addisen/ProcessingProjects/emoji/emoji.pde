size(200, 200);
fill(255,255,0);
//draw the head
ellipse(100,100,150,150);
//white
fill(255);
//draw the eyes
ellipse(75,85,30,40);
ellipse(125,85,40,30);
fill(255,255,255);
//draw the pupils
ellipse(75,85,10,10);
ellipse(125,85,10,10);
//red
fill(255,0,255);
//draw the mouth
arc(100,125,80,50,0,3.14);
line(60,125,140,125);