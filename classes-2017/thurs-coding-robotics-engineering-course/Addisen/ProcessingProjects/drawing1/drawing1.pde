int weight=1;

void setup() {
  size(400, 400);
  frameRate(2000);
  background(0);
}

void draw() {
  stroke(random(255), random(255), random(255));
  strokeWeight(weight);
  line( pmouseX, pmouseY, mouseX, mouseY);
}

void mousePressed() {
  stroke(0);
  fill(random(255), random(255), random(255));
  rectMode(CENTER);
  if (mouseButton==37) {
    rect(mouseX, mouseY, 20, 20);
    rect(mouseX, mouseY, 15, 15);
    rect(mouseX, mouseY, 10, 10);
    rect(mouseX, mouseY, 5, 5);
  }
  println (mouseButton);
  if (mouseButton==39) {
    rect(mouseX, mouseY, 0, 0);
  }
}
void keyPressed() {
  println(keyCode);
  println(key);
  if (keyCode==61) {
    weight= weight + 1;
  }
  if (keyCode==45) {
    weight= weight - 1;
  }
}